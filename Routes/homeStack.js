import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Home } from '../components/home';
import { ViewDetail } from '../components/viewdetail';
// import { createAppContainer } from 'react-navigation';
import Header from '../shared/header';

const screens = {
    Home: {
        screen: Home,
        navigationOptions: ({ navigation, title }) => {
            return {
                headerTitle: () => <Header navigation={navigation} title='GameZoon' />
            }
        }
            // {
            // // title: 'Home',
            // headerTitle: () => <Header />,
            // headerTintColor: 'blue',
            // headerStyle: {
            //     backgroundColor: '#bbb',
            // }
    },
    ViewDetail: {
        screen: ViewDetail,
        navigationOptions: {
            title: 'ViewDetail',
            headerTintColor: '#fff',
            headerStyle: {
                backgroundColor: 'coral',
            }
        }
    },
}
const HomeStack = createStackNavigator(screens, {
    defaultNavigationOptions: {
        headerTintColor: "#444",
        headerStyle: {
            backgroundColor: '#eee',
            height: 60,
        }
    }
});

export default HomeStack;
