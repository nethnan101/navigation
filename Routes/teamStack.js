import { createStackNavigator } from 'react-navigation-stack';
import { Team } from '../components/team';
import React from 'react';
import Header from '../shared/header';

const screens = {
    Team: {
        screen: Team,
        navigationOptions: ({ navigation, title }) => {
            return {
                headerTitle: () => <Header navigation={navigation} title='Team Zoon' />
            }
        }
        // navigationOptions: {
        //     title: 'About Game Zoon',
        //     // headerTintColor: 'blue',
        //     headerStyle: {
        //         backgroundColor: '#bbb',
        //     }
        // }
    }
}
const TeamStack = createStackNavigator(screens);

export default TeamStack;