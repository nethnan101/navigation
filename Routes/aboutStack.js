import { createStackNavigator } from 'react-navigation-stack';
import { About } from '../components/about';
import React from 'react';
import Header from '../shared/header';

const screens = {
    About: {
        screen: About,
        navigationOptions: ({ navigation , title}) => {
            return {
                headerTitle: () => <Header navigation={navigation} title='About'/>
            }
        }
        // navigationOptions: {
        //     headerTitle: () => <Header />,
        //     // title: 'About Game Zoon',
        //     // headerTintColor: 'blue',
        //     headerStyle: {
        //         backgroundColor: '#bbb',
        //     }
        // }
    }
}
const AboutStack = createStackNavigator(screens);

export default AboutStack;
