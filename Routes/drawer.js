import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';


import AboutStack from './aboutStack';
import HomeStack from './homeStack';
import TeamStack from './teamStack';

const RootDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: HomeStack,
    },
    About: {
        screen: AboutStack,
    },
    Team:{
        screen: TeamStack,
    }
});

export default createAppContainer(RootDrawerNavigator);