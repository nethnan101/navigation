import React from 'react';
import {StyleSheet, View, Text, TextInput, Button, TouchableWithoutFeedback, Keyboard} from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';

// condition for before submit Formik
const schema = yup.object({
    title: yup.string()
        .required()
        .min(4),
    pos: yup.string()
        .required()
        .min(4),
    body: yup.string()
        .required()
        .min(4),
    rating: yup.string()
        .required()
        .test('is-num-1-3', 'Rating must be a number 1 - 3', (val) => {
            return parseInt(val) < 4 && parseInt(val) > 0;
    })
})

export const AddUser = ({AddCustomer}) => {
    return (
        <View style={styles.container}>
            <Formik
                style={styles.formik}
                initialValues={{ 
                    title:'',
                    pos: '',
                    body: '',
                    rating: '',
                }}
                validationSchema={schema}
                onSubmit={(values, actions) => {
                    actions.resetForm(values);
                    AddCustomer(values);
                    console.log(values);
                }}
            >
                {(props) => (
                    <TouchableWithoutFeedback 
                        onPress={Keyboard.dismiss} 
                        // accessible={false}
                    >
                    <View style={styles.content}>
                        <Text style={styles.textAdd}>Add New Customer</Text>
                        <TextInput 
                            style={styles.inputText}
                            placeholder='Full Name'
                            onChangeText={props.handleChange('title')}
                            value={props.values.title}
                            onBlur={props.handleBlur('title')}
                        />
                        <Text style={styles.errorsText}>{props.touched.title && props.errors.title}</Text>
                        <TextInput 
                            multiline
                            style={styles.inputText}
                            placeholder='Problems'
                            onChangeText={props.handleChange('pos')}
                            value={props.values.pos}
                            onBlur={props.handleBlur('pos')}
                        />
                        <Text style={styles.errorsText}>{props.touched.pos && props.errors.pos}</Text>
                        <TextInput 
                            multiline
                            style={styles.inputText}
                            placeholder='Discription'
                            onChangeText={props.handleChange('body')}
                            value={props.values.body}
                            onBlur={props.handleBlur('body')}
                        />
                        <Text style={styles.errorsText}>{props.touched.body && props.errors.body}</Text>
                        <TextInput 
                            style={styles.inputText}
                            placeholder='Rating (1-3)'
                            onChangeText={props.handleChange('rating')}
                            value={props.values.rating}
                            keyboardType='numeric'
                            onBlur={props.handleBlur('rating')}
                        />
                        <Text style={styles.errorsText}>{props.touched.rating && props.errors.rating}</Text>
                        <View style={styles.buttonSubmit}>
                            <Button 
                                title='Submit' 
                                color= '#fff'
                                // color='maroon'
                                onPress={props.handleSubmit}
                            />
                        </View>
                    </View>
                    </TouchableWithoutFeedback>
                )}
            </Formik>
        </View>
    )
}


const styles = StyleSheet.create({
    container:{
        top: 0,
    },
    formik:{
        top:0
    },
    content: {
        width: '100%',
    },
    inputText: {
        padding: 10,
        borderRadius: 5,
        color: '#000',
        borderWidth: 1,
        borderColor: 'maroon',
        fontSize: 18,
        margin: 10,
    },
    textAdd:{
        textAlign: 'center',
        fontSize: 28,
        fontWeight: 'bold',
        color: '#898',
    },
    buttonSubmit: {
        backgroundColor: 'maroon',
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        width: '95%',  
        height: 50,    
    },
    errorsText: {
        color: 'red',
        // fontWeight: 'bold',
        paddingLeft: 12,
    }
})