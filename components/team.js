import React from 'react'
import { View, Text, StyleSheet, } from 'react-native'

export const Team = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Team Screen</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
    },
    text:{
        paddingTop: 50,
        fontSize: 25,
        textAlign: 'center'
    }
})