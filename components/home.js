import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, FlatList,  Modal, TouchableWithoutFeedback, Keyboard} from 'react-native'
import { MaterialIcons, AntDesign, Ionicons } from '@expo/vector-icons';
import { AddUser } from './AddUser';
import { CurrentRenderContext } from '@react-navigation/native';

export const Home = ({ navigation }) => {
    const [datas, setDatas] = useState([
        {title: 'Neth', rating: 1, pos: 'Code Error cannot solve', body: 'Single', key: 1},
        {title: 'Sovith',rating: 2, pos: 'Love hot girl' , body: 'Married' , key: 2},
        {title: 'Samleng', rating: 3,  pos: 'Have a lot of girlfriends', body: 'Lecturer', key: 3},
        {title:'Kaka' , rating: 2, pos: 'Beautiful girl', body: 'Student' , key: 4}
    ])
    const [modal, setModal] = useState(false);
    const onDelete =(key) => {
        setDatas((prevDatas) => {
            console.log('Datas have been deletes');
            return prevDatas.filter(data => data.key=!key)
        })
        // return navigation.navigate('Home');
    }
    const AddCustomer = (user) => {
        user.key = Math.random().toString();
        setDatas((currentDatas) => {
            return[ user, ...currentDatas]
        })
        setModal(false)
    }
    // const pressHandler = () => {
    //     navigation.navigate('ViewDetail');
    //     // navigation.push('ViewDetail');
    // }
    return (
        <View style={styles.container}>
            {/* <Text style={styles.text}>Home Screen</Text>
            <Text></Text> */}
            {/* <TouchableOpacity onPress={pressHandler} style={styles.buttonTouch}>
                <Button 
                    color='#000'
                    onPress={pressHandler} 
                    title='Go to ViewDetail'
                />
            </TouchableOpacity> */}
            
            <Modal visible={modal} animationType='slide'>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss()}>
                    <View style={styles.modalview}>
                        <AntDesign 
                            name="close" 
                            style={styles.modalclose} 
                            onPress={() => setModal(false)}
                        />
                        <Text></Text>
                        <AddUser AddCustomer={AddCustomer} />
                    </View> 
                </TouchableWithoutFeedback>   
            </Modal>

            <View style={styles.viewitem}>
                <FlatList 
                    data={datas}
                    // keyExtractor={item => item.id}
                    renderItem={({item}) => (
                        <TouchableOpacity onPress={() => navigation.navigate('ViewDetail', item)} style={styles.item}>
                            <Text style={styles.text}>{item.title}</Text>
                            <MaterialIcons style={styles.icon} name='navigate-next' />
                        </TouchableOpacity>
                    )}
                />
            </View>

            <Ionicons
                name="add-circle"  
                // color="black" 
                style={styles.adduser}
                onPress={() => setModal(true)}
            />
            
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
    },
    text:{
        // paddingTop: 50,
        fontSize: 25,
        textAlign: 'left',
        // backgroundColor: '#eee'
        color: '#fff', 
        flex: 1
    },
    buttonTouch: {
        width: '50%',
        textAlign: 'center',
        backgroundColor: '#bbb',
        borderRadius: 20,
        borderWidth: 1,
        borderColor: 'blue',
        alignSelf: 'center',
    },
    item:{
        padding: 20,
        borderBottomColor: '#eee',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: 'green',
        flexDirection: 'row',
        flex: 1
    },
    viewitem:{
        height: '100%'
    },
    icon:{
        color: '#fff',
        fontSize: 35,       
        flexDirection: 'row',
    },
    modalview: {
        // flex: 1,
        // paddingTop: 30,
        // flexDirection: 'column',
    },
    adduser: {
        position: 'absolute',
        // position: 'sticky'
        fontSize: 60, 
        right: 10, 
        bottom: 10,
        color: '#166AD3',
    },
    modalclose:{
        fontSize: 40,
        alignSelf: 'flex-end',
        paddingTop: 30
    },
})
