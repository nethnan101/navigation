import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Button, Image } from 'react-native'
import { images } from '../styles/globlestyle';
// import { MaterialCommunityIcons } from '@expo/vector-icons';

export const ViewDetail = ({ navigation, onDelete }) => {
    // const pressGoBack =() => {
    //     navigation.goBack();
    // }

    const rating = navigation.getParam('rating');

    return (
        <View style={styles.container}>
            {/* <Text style={styles.text}>ViewDetails Screen</Text>
            <TouchableOpacity onPress={pressGoBack} style={styles.buttontouch} >
                <Button 
                    onPress={pressGoBack}
                    title='Go back'
                    color='#fff'
                    />
            </TouchableOpacity> */}
            {/* <MaterialCommunityIcons 
                name="delete-outline" 
                size={30} 
                color="black" 
                onPress={onDelete}
            /> */}
            <Text style={styles.listparam}>{navigation.getParam('title')}</Text>
            <Text style={styles.listparam}>{navigation.getParam('body')}</Text>
            <Text style={styles.listparam}>{navigation.getParam('pos')}</Text>
            <View style={styles.imageView}>
                <Image 
                    style={styles.image}
                    source={images.ratings[rating]} 
                />
                {/* <Text style={styles.listparam}>{navigation.getParam('rating')}</Text> */}
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    text:{
        // paddingTop: 50,
        fontSize: 25,
        textAlign: 'center'
    },
    buttontouch: {
        backgroundColor: 'blue',
        borderWidth:1,
        borderColor: '#bbb',
        width: '50%',
        alignSelf: 'center',
        borderRadius: 20
    },
    listparam:{
        paddingLeft: 20,
        padding: 2,
        fontSize: 18,
    }, 
    imageView: {
        flexDirection: 'column',
        position: 'relative',
        width: '100%'
    },
    image: {
        width: '100%',
        height: '100%',
    }
})