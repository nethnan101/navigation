import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import { Entypo } from '@expo/vector-icons';

export default function Header({navigation, title}) {
    const menu = () => {
        navigation.openDrawer();
    }
    return(
        <View style={styles.container}>
            {/* icon menu */}
            <Entypo name="menu" onPress={menu} color="black" style={styles.icon} />
            <View>
                <Text style={styles.headerText}>{title}</Text>
            </View>    
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText:{
        fontSize: 20,
        fontWeight: 'bold',
        color: '#333',
        letterSpacing: 1
    },
    icon:{
        position: 'absolute',
        fontSize: 30,
        left: -65,
    }
})